# Markdown Editor (React)

## Description :
* Editeur de Markdown crée via React.
* Permet de visualiser directement le rendu de son Markdown

## Technologies utilisées :
* React
* CSS 3

## Visualiser le projet : 
* [Cliquez ici](https://julie-desvaux.github.io/react_markdown_editor/)

## 
![Visuel](http://julie-desvaux.com/images/markdown_editor.png "Visuel")
